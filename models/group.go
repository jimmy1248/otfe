package models

import (
	"github.com/globalsign/mgo/bson"
)

//Group type
type Group struct {
	ID          bson.ObjectId   `bson:"_id,omitempty"`
	Name        string          `bson:"name"`
	Permissions map[string]bool `bson:"permissions"`
	Admin       bool            `bson:"admin"`
	Users       []bson.ObjectId `bson:"users"`
}

func NewGroup(Name string) Group {
	var group Group
	group.Permissions = make(map[string]bool)
	return group
}

//Create group
func (g *Group) Create() error {
	return create("group", &g)
}

//Read group
func (g *Group) Read(key string, value interface{}) error {
	return read("group", key, value, &g)

}

//ReadAll groups
func (g *Group) ReadAll() ([]Group, error) {
	var groups []Group
	var err error
	err = readAll("group", "", nil, &groups)
	return groups, err
}

//Update group
func (g *Group) Update() error {
	return update("group", "name", g.Name, &g)
}

//Delete group
func (g *Group) Delete(key string, value string) error {
	err := delete("groups", key, value)
	return err
}


