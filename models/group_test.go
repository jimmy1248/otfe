package models

import (
	"testing"

	"github.com/globalsign/mgo/bson"
)

func TestCreateGroup(t *testing.T) {
	group := NewGroup("test")
	group.Users = append(group.Users, bson.NewObjectId())
	group.Permissions["test"] = true
	t.Log(group.Create())
}

func TestReadGroup(t *testing.T) {
	var group Group
	group.Read("name", "test")
	t.Log(group)
}

func TestReadAllGroup(t *testing.T) {
	var group Group
	t.Log(group.ReadAll())
}
