package models

import (
	"testing"

	"github.com/globalsign/mgo/bson"

	"golang.org/x/crypto/bcrypt"
)

func TestSeed(t *testing.T) {
	DBWipeCollection("user")
	password1, _ := hashPassword("a")
	user1 := User{Username: "Bob", Name: "Bob", Email: "bob@bob.com", Password: password1}
	t.Log(user1.Create())
	user2 := User{Username: "Fred", Name: "Fred", Email: "b"}
	user2.Create()
	user3 := User{Username: "Lucy", Name: "Lucy", Email: "c"}
	user3.Create()
	user4 := User{Username: "Ann", Name: "Ann", Email: "d"}
	user4.Create()
	user5 := User{Username: "Ted", Name: "Ted", Email: "e"}
	user5.Create()
	user6 := User{Username: "Egor", Name: "Egor", Email: "f"}
	user6.Create()
}

func TestCreateUser(t *testing.T) {
	user := User{Email: "iojkmiojko", Username: "rytiuhmhhjm,",
		ID: bson.NewObjectId()}
	t.Log(user.Create())
}

func TestUserRead(t *testing.T) {
	var user User
	t.Log(user.Read("name", "Ann"))
	t.Log(user)
}

func TestUserReadAll(t *testing.T) {
	var user User
	t.Log(user.ReadAll())
}

func TestHasPermission(t *testing.T) {
	c, s := GetCollection("user")
	c.RemoveAll(nil)
	s.Close()
	c, s = GetCollection("group")
	c.RemoveAll(nil)
	s.Close()
	group := NewGroup("test")
	group.ID = bson.NewObjectId()
	group.Create()
	user := User{Name: "user", Email: "a", PrimaryGroup: group.ID}
	user.Create()
	//Check no permission
	if user.HasPermission("") {
		t.Fail()
	}
	//Check admin permission
	group.Admin = true
	group.Update()
	if !user.HasPermission("") {
		t.Fail()
	}
	group.Admin = false
	//Check permission
	group.Permissions["perm"] = true
	group.Update()
	if !user.HasPermission("perm") {
		t.Fail()
	}
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}
