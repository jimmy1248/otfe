package models

import (
	"testing"
)

func TestGetSession(t *testing.T) {
	GetMongoSession()
}

func TestGetCollection(t *testing.T) {
	GetCollection("test")
}

func TestCreate(t *testing.T) {
	create("user", &User{Name: "Ale"})
}

func TestReadAll(t *testing.T) {
	var u []User
	readAll("user", "", nil, &u)
	t.Log(u)
}

func TestRead(t *testing.T) {
	var u User
	read("user", "name", "Ann", &u)
	t.Log(u)
}

func TestUpdate(t *testing.T) {
	update("test", "name", "Ale", &User{Name: "Bob", Email: "z"})
}

func TestDelete(t *testing.T) {
	t.Log(delete("user", "name", "Ann"))
}


