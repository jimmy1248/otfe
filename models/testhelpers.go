package models

//DBWipeCollection removes all data from the
//specifed collections
func DBWipeCollection(collections ...string) {
	for _, collection := range collections {
		c, s := GetCollection(collection)
		defer s.Close()
		c.RemoveAll(nil)
	}
}
