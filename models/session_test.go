package models

import (
	"net/http"
	"testing"

	"git.1248.nz/1248/Otfe/helpers"
	"git.1248.nz/1248/Otfe/helpers/cookie"
	"github.com/globalsign/mgo/bson"
)

func TestSessionCreate(t *testing.T) {
	var s1, s2 Session
	if s1.Create() != nil {
		t.Fail()
	}
	read("session", "_id",
		s1.ID, &s2)
	if s1.ID != s2.ID {
		t.Fail()
	}
}

func TestSessionRead(t *testing.T) {
	var s1, s2 Session
	s1.ID = bson.NewObjectId()
	create("session", &s1)
	if s2.Read(s1.ID.Hex()) != nil {
		t.Fail()
	}
	if s1.ID != s2.ID {
		t.Fail()
	}
}

func TestGet(t *testing.T) {
	DBWipeCollection("session")
	var s1, s2 Session
	s1.ID = bson.NewObjectId()
	s1.Create()
	c := &http.Cookie{Name: "session",
		Value: cookie.Encode(s1.ID.Hex())}
	request, err := http.NewRequest("GET", "/", nil)
	helpers.Ok(t, err)
	request.AddCookie(c)
	s2.Get(request)
	helpers.Equals(t, s1, s2)
}
