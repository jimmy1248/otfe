package models

import (
	"net/http"
	"time"

	"git.1248.nz/1248/Otfe/helpers"
	"git.1248.nz/1248/Otfe/helpers/cookie"
	"github.com/globalsign/mgo"

	"github.com/globalsign/mgo/bson"
)

func init() {
	collection, session := GetCollection("session")
	defer session.Close()
	collection.EnsureIndex(mgo.Index{ExpireAfter: 1200})
}

//Session model
type Session struct {
	ID          bson.ObjectId `bson:"_id,omitempty"`
	UserID      bson.ObjectId `bson:"userid,omitempty"`
	LogininTime time.Time     `bson:"createdAt"`
	LastSeenTme time.Time
	ExpireAt    time.Time `bson:"expireAt"`
	IP          string
}

//Create session and set LoginTime
func (s *Session) Create() error {
	s.LogininTime = time.Now()
	s.ID = bson.ObjectIdHex(helpers.RandHex())
	return create("session", &s)
}

//Read session
func (s *Session) Read(id string) error {
	return read("session", "_id", bson.ObjectIdHex(id), &s)

}

//Update LastSeenTime
func (s *Session) Update() error {
	s.LastSeenTme = time.Now()
	return update("session", "_id", s.ID, &s)
}

//Delete session
func (s *Session) Delete(id string) error {
	return delete("session", "_id", bson.ObjectIdHex(id))
}

//Get session from http request and check if it is valid
func (s *Session) Get(r *http.Request) error {
	//Read session cookie
	id, err := cookie.Read(r, "session")
	if err == nil {
		err = s.Read(id)
	}
	return err
}
