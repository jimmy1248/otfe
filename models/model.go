package models

import (
	"log"

	"git.1248.nz/1248/Otfe/helpers/config"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

var session *mgo.Session

func init() {
	GetMongoSession()

}

// Creates a new session if mgoSession is nil i.e there is no active mongo session.
//If there is an active mongo session it will return a Clone
func GetMongoSession() *mgo.Session {
	if session == nil {
		var err error
		session, err = mgo.Dial(config.Get().DB.Host)
		if err != nil {
			log.Fatal("Failed to start the Mongo session")
		}
	}
	session.SetMode(mgo.Monotonic, true)
	return session.Clone()
}

func GetCollection(collectionName string) (*mgo.Collection, *mgo.Session) {
	session := GetMongoSession()
	return session.DB(config.Get().DB.Name).C(collectionName), session
}

func create(c string, data interface{}) error {
	collection, session := GetCollection(c)
	defer session.Close()
	return collection.Insert(data)
}

func readAll(c string, key string, value interface{}, result interface{}) error {
	collection, session := GetCollection(c)
	defer session.Close()
	if key == "" {
		return collection.Find(nil).All(result)
	}
	return collection.Find(bson.M{key: value}).All(result)

}

func read(c string, key string, value interface{}, result interface{}) error {
	collection, session := GetCollection(c)
	defer session.Close()
	return collection.Find(bson.M{key: value}).One(result)
}

func update(c string, key string, value interface{}, data interface{}) error {
	collection, session := GetCollection(c)
	defer session.Close()
	return collection.Update(bson.M{key: value}, data)
}

func delete(c string, key string, value interface{}) error {
	collection, session := GetCollection(c)
	defer session.Close()
	return collection.Remove(bson.M{key: value})

}
