{{define "content"}}
<h1>{{.Title}}</h1>
<ul>
    {{range $i, $a := .Users}}
        <li>
            <a href="/user/{{$a.Username}}">{{$a.Username}}</a>
        </li>
    {{end}}
</ul>

<form action="/user/new">
    <button type="submit">New User</button>
</form>
{{end}}



