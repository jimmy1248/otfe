{{define "content"}}

<h1>{{.User.Name}}</h1>
<h2>Username: {{.User.Username}}</h2>
<h2>Email:  {{.User.Email}}</h2>
<h2>Password: {{.User.Password}}</h2>
<form action="/user/{{.User.Username}}/delete" method="post">
    <button type="submit">Delete</button>
</form>
<h1>{{getId .User.ID}}</h1>
{{end}}