{{define "content"}}
<form action="/user/new" method="post">
    <fieldset>
        <legend>New User</legend>
        <div>Username</div>
        <input type="text" name="username">
        <div>Email</div>
        <input type="email" name="email">
        <div>Password</div>
        <input type="password" name="password">
        <div>Password Repeat</div>
        <input type="password" name="password-repeat"><br>
        <input type="submit" value="Submit">
    </fieldset>
</form>

{{end}}