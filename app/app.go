package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/husobee/vestigo"
)

func main() {
	router := vestigo.NewRouter()
	routes(router)
	fmt.Println("Starting")
	log.Fatal(http.ListenAndServe(":8080", router))
}
