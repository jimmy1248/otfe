package main

import (
	"testing"

	"git.1248.nz/1248/Otfe/helpers"
	"git.1248.nz/1248/Otfe/models"
	"github.com/globalsign/mgo/bson"
)

func TestSeed(t *testing.T) {
	models.DBWipeCollection("group", "user", "session")
	//admin user and group
	adminGroup := models.NewGroup("admin")
	adminGroup.Admin = true
	adminGroup.ID = bson.NewObjectId()
	adminGroup.Permissions["user.show"] = true

	admin := models.User{}
	admin.Username = "admin"
	admin.Email = "a"
	admin.ID = bson.NewObjectId()
	admin.Password, _ = helpers.HashPassword("admin")
	admin.PrimaryGroup = adminGroup.ID
	adminGroup.Users = append(adminGroup.Users, admin.ID)
	helpers.Ok(t, adminGroup.Create())
	helpers.Ok(t, admin.Create())

	//user and user group
	userGroup := models.NewGroup("user")
	userGroup.ID = bson.NewObjectId()
	userGroup.Admin = false
	user := models.User{}
	user.ID = bson.NewObjectId()
	user.Username = "user"
	user.Email = "u"
	user.Password, _ = helpers.HashPassword("user")
	user.PrimaryGroup = userGroup.ID
	userGroup.Users = append(userGroup.Users, user.ID)
	helpers.Ok(t, user.Create())
	helpers.Ok(t, userGroup.Create())

}
