package main

import (
	"net/http"

	c "git.1248.nz/1248/Otfe/controllers"
	"git.1248.nz/1248/Otfe/helpers"
	a "git.1248.nz/1248/Otfe/helpers/auth"
	"github.com/husobee/vestigo"
)

func routes(router *vestigo.Router) {

	var static c.Static
	router.Get("/", a.User(static.Home))

	router.Get("/public/*", http.FileServer(
		http.Dir(helpers.GetAssets())).ServeHTTP)

	//User routes
	var user c.User
	router.Get("/user", user.Index)
	router.Get("/user/:username", a.Perm(user.Show, user.ShowSelf,
		"user.show"))
	router.Get("/user/new", user.New)
	router.Post("/user/new", user.Create)
	router.Get("/user/:username/edit", user.Edit)
	router.Post("/user/:username/edit", user.Update)
	router.Post("/user/:username/delete", user.Delete)
	router.Get("/register", user.New)

	//Session routes
	var session c.Session
	router.Get("/login", session.New)
	router.Post("/login", session.Create)
	router.Post("/logout", session.Delete)
}
