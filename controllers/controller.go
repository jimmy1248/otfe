package controllers

import (
	"html/template"
	"net/http"

	"git.1248.nz/1248/Otfe/helpers"
	"github.com/globalsign/mgo/bson"
)

/*type Controller interface {
	Index(w http.ResponseWriter, r *http.Request)
	Show(w http.ResponseWriter, r *http.Request)
	New(w http.ResponseWriter, r *http.Request)
	Create(w http.ResponseWriter, r *http.Request)
	Edit(w http.ResponseWriter, r *http.Request)
	Update(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
}*/

var funcMap = template.FuncMap{
	"getId": func(id bson.ObjectId) string {
		return "1"
	},
}

func t(w http.ResponseWriter, data interface{}, layout string) {
	views := helpers.GetRootDir() + "/views/"
	tmpl := template.Must(template.New("layout").Funcs(funcMap).
		ParseFiles(views+"/layouts/layout.gtpl", views+"/layouts/header.gtpl", views+"/layouts/footer.gtpl", views+"/layouts/nav.gtpl", views+layout))
	tmpl.ExecuteTemplate(w, "layout", data)

}
