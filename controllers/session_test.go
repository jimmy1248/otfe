package controllers

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestNew(t *testing.T) {
	var s Session
	handler := http.HandlerFunc(s.New)
	req, err := http.NewRequest("GET", "/login", nil)
	w := httptest.NewRecorder()
	if err != nil {
		t.Fatal(err)
	}
	handler(w, req)
	body := w.Body.String()
	if !strings.Contains(body, "<title>Login</title>") {
		t.Fail()
	}
}
