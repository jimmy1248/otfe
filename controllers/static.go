package controllers

import (
	"net/http"

	"git.1248.nz/1248/Otfe/models"
)

//Static pages
type Static struct{}

type staticData struct {
	Title string
	User  models.User
}

type contextKey string

func (c contextKey) String() string {
	return string(c)
}

//Home page
func (s *Static) Home(w http.ResponseWriter, r *http.Request, u models.User) {
	data := staticData{Title: "Otfe"}
	data.User = u
	//fmt.Fprintln(w, data.User)
	t(w, data, "/static/home.gtpl")
}
