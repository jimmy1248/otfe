package controllers

import (
	"errors"
	"net/http"

	"git.1248.nz/1248/Otfe/helpers"
	"git.1248.nz/1248/Otfe/helpers/cookie"
	"git.1248.nz/1248/Otfe/models"

	"github.com/globalsign/mgo/bson"
)

//Session controllers
type Session struct{}

type pageData struct {
	Title string
	Err   string
	User  models.User
}

//New login form
func (s *Session) New(w http.ResponseWriter, r *http.Request) {
	var err error
	data := pageData{Title: "Login"}
	data.Err, err = cookie.Read(r, "error")
	if err == nil {
		cookie.Delete(w, "error")
	}
	t(w, data, "/static/login.gtpl")
}

//Create a new session
func (s *Session) Create(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	//Get email and password and check they are not empty
	email := r.Form.Get("email")
	password := r.Form.Get("password")
	if email == "" || password == "" {
		loginFail(w, r, errors.New("Please enter an email and password"))
		return
	}

	//Check if user exists
	var user models.User
	err := user.Read("email", email)
	if err != nil {
		err := user.Read("username", email)
		if err != nil {
			loginFail(w, r, errors.New("Email or password incorrect"))
			return
		}
	}

	//Check password is correct
	if helpers.CheckPasswordHash(password, user.Password) == nil {
		sess := models.Session{ID: bson.NewObjectId(), UserID: user.ID}
		sess.Create()
		cookie.Create(w, "session", sess.ID.Hex())
		http.Redirect(w, r, "/", http.StatusFound)
	} else {
		loginFail(w, r, errors.New("Email or password incorrect"))

	}
}

//Delete session
func (s *Session) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := cookie.Read(r, "session")
	//Check user is logged in
	if err == nil {
		cookie.Delete(w, "session")
		var session models.Session
		session.Delete(id)
		http.Redirect(w, r, "/", http.StatusFound)
	}

}

func loginFail(w http.ResponseWriter, r *http.Request, err error) {
	cookie.Create(w, "error", err.Error())
	http.Redirect(w, r, "/login", http.StatusFound)
}
