package config

import (
	"encoding/hex"
	"path/filepath"

	"git.1248.nz/1248/Otfe/helpers"

	"github.com/BurntSushi/toml"
)

//Configuration struct
type Configuration struct {
	DB      database `toml:"database"`
	Session session
}

// Database stuct
type database struct {
	Host     string
	Name     string
	User     string
	Password string
}

type session struct {
	SecretKey  string
	Sessionkey string
	Timeout    int
}

var config *Configuration

func init() {
	Get()
}

// Get config info from toml config file
func Get() *Configuration {
	if config == nil {
		_, err := toml.DecodeFile(getConfigFile(), &config)
		helpers.CheckError(err)
	}
	return config
}

func getConfigFile() string {
	return filepath.Join(helpers.GetRootDir(), "config.toml")
}

func GetSecretKey() []byte {
	config := Get()
	key, err := hex.DecodeString(config.Session.SecretKey)
	helpers.CheckError(err)
	return key
}

func GetSessionKey() string {
	return Get().Session.Sessionkey
}
