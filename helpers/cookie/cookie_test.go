package cookie

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"git.1248.nz/1248/Otfe/helpers"
)

func TestCreate(t *testing.T) {
	recorder := httptest.NewRecorder()
	Create(recorder, "test", "test")
	request := &http.Request{Header: http.Header{
		"Cookie": recorder.HeaderMap["Set-Cookie"]}}
	cookie, err := request.Cookie("test")
	if err != nil {
		t.Fail()
		return
	}
	value, err := Decode(cookie.Value)
	if err != nil || value != "test" {
		t.Fail()
	}
}

func TestRead(t *testing.T) {
	cookie := &http.Cookie{Name: "test", Value: Encode("test")}

	request, err := http.NewRequest("GET", "", nil)
	if err != nil {
		t.Fail()
		return
	}
	request.AddCookie(cookie)
	value, err := Read(request, "test")
	helpers.Equals(t, value, "test")
}
