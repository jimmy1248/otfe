package cookie

import (
	"encoding/base64"
	"errors"
	"net/http"
	"time"
)

func Create(w http.ResponseWriter, name string, value string) {
	c := &http.Cookie{Name: name, Value: Encode(value)}
	http.SetCookie(w, c)
}

func Read(r *http.Request, name string) (string, error) {
	c, err := r.Cookie(name)
	if err != nil {
		return "", errors.New("Cookie not found")
	}
	value, err := Decode(c.Value)
	if err != nil {
		return "", errors.New("Failed to decode cookie")
	}
	return value, nil
}

func Delete(w http.ResponseWriter, name string) {
	http.SetCookie(w, &http.Cookie{Name: name, MaxAge: -1, Expires: time.Unix(1, 0)})

}

func Encode(src string) string {
	return base64.URLEncoding.EncodeToString([]byte(src))
}

func Decode(src string) (string, error) {
	value, err := base64.URLEncoding.DecodeString(src)
	return string(value), err
}
