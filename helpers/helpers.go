package helpers

import (
	"crypto/rand"
	"encoding/hex"
	"log"
	"path/filepath"
	"runtime"

	"golang.org/x/crypto/bcrypt"
)

//CheckError checks for errors and logs them and stops the program
func CheckError(err error) bool {
	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

func GetRootDir() string {
	_, b, _, _ := runtime.Caller(0)
	dir := filepath.Dir(b)
	return filepath.Dir(dir)
}

func GetAssets() string {
	return GetRootDir()
}

func HashPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(hash), err
}

func CheckPasswordHash(password, hash string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err
}

func RandHex() string {
	bytes := make([]byte, 12)
	rand.Read(bytes)
	return hex.EncodeToString(bytes)
}

func Bytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}
