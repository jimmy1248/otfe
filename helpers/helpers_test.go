package helpers_test

import (
	"testing"

	"git.1248.nz/1248/Otfe/helpers"
	"git.1248.nz/1248/Otfe/models"
)

func TestGetRootDir(t *testing.T) {
	t.Log("Root path:", helpers.GetRootDir())
}

func TestHashPassword(t *testing.T) {
	user := models.User{Email: "a@a.com", Username: "a"}
	user.Delete("username", "a")
	var err error
	password := "43539jgifdkvnm4935078uJKJR**$ufjqd98438uiAHFJean89q34JKDFJ"
	user.Password, err = helpers.HashPassword(password)
	if err != nil {
		t.Fail()
	}
	user.Create()
	var user2 models.User
	user2.Read("username", "a")

	t.Log(helpers.CheckPasswordHash(password, user2.Password))

}

func TestRandHex(t *testing.T) {
	for i := 0; i < 20; i++ {
		t.Log(helpers.RandHex())
	}
}
